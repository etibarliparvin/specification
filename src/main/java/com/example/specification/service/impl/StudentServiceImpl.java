package com.example.specification.service.impl;

import com.example.specification.dto.response.CommonResponse;
import com.example.specification.entity.Student;
import com.example.specification.repository.StudentRepository;
import com.example.specification.search.student.SearchFilter;
import com.example.specification.search.student.SearchSpecification;
import com.example.specification.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;


    @Override
    public CommonResponse search(SearchFilter filter) {
        CommonResponse response = new CommonResponse();
        SearchSpecification specification = new SearchSpecification();
        specification.search(filter);
        List<Student> students = studentRepository.findAll(specification);
        response.setItem(students);
        return response;
    }

    @Override
    public CommonResponse getAll() {
        CommonResponse response = new CommonResponse();
        List<Student> all = studentRepository.findAll();
        response.setItem(all);
        return response;
    }
}
