package com.example.specification.service;

import com.example.specification.dto.response.CommonResponse;
import com.example.specification.search.student.SearchFilter;

public interface StudentService {

    CommonResponse search(SearchFilter filter);

    CommonResponse getAll();
}
