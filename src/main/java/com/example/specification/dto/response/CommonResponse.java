package com.example.specification.dto.response;

import lombok.Data;

@Data
public class CommonResponse {
    private Long time = System.currentTimeMillis();
    private String status = "Success";
    private Object item;
    private CommonMessage errorMessage;
}
