package com.example.specification.dto.response;

import lombok.Data;

@Data
public class CommonMessage {
    private String message;
    private String code;
    private String description;
}
