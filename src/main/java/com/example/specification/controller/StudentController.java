package com.example.specification.controller;

import com.example.specification.repository.StudentRepository;
import com.example.specification.search.student.SearchFilter;
import com.example.specification.search.student.SearchMappingFilter;
import com.example.specification.service.StudentService;
import jakarta.persistence.GeneratedValue;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDate;

@RestController
@RequiredArgsConstructor
@RequestMapping("/student")
public class StudentController {

    private final StudentService studentService;
    private final SearchMappingFilter filter;

    @GetMapping("/search")
    public ResponseEntity<?> search(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String teacherName) {
        return ResponseEntity.ok(studentService.search(filter.setMapFilter(name, teacherName)));
    }
}
