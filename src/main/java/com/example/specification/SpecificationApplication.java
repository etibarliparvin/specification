package com.example.specification;

import com.example.specification.entity.Student;
import com.example.specification.repository.StudentRepository;
import com.example.specification.search.student.SearchCriteria;
import com.example.specification.search.student.SearchOperation;
import com.example.specification.search.student.SearchSpecification;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class SpecificationApplication implements CommandLineRunner {

    private final StudentRepository studentRepository;

    public static void main(String[] args) {
        SpringApplication.run(SpecificationApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Start------------------------------------------------");

//        SearchSpecification specification = new SearchSpecification();
//        specification.add(SearchCriteria.builder()
//                .key("gender")
//                .value1("female")
//                .operation1(SearchOperation.JOIN_TEACHERS)
//                .operation2(SearchOperation.EQUAL)
//                .build());
//
//        List<Student> all = studentRepository.findAll(specification);
//        all.stream().forEach(System.out::println);

        System.out.println("Finish-----------------------------------------------");
    }
}
