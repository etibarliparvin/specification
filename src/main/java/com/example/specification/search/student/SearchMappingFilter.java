package com.example.specification.search.student;

import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.time.LocalDate;

@Component
public class SearchMappingFilter {

    public SearchFilter setMapFilter(
//            Integer page,
//            Integer size,
            String name,
//            String surname,
//            String pinCode,
//            Double gpa,
//            LocalDate birthdate,
//            BigDecimal minScholarship,
//            BigDecimal maxScholarship,
//            String country,
//            String city,
            String teacherName
    ) {
        SearchFilter filter = new SearchFilter();
//        filter.setPage(page);
//        filter.setSize(size);
        if (!ObjectUtils.isEmpty(name))
            filter.setName(name);
//        if (!ObjectUtils.isEmpty(surname))
//            filter.setSurname(surname);
//        if (!ObjectUtils.isEmpty(pinCode))
//            filter.setPinCode(pinCode);
//        if (!ObjectUtils.isEmpty(gpa))
//            filter.setGpa(gpa);
//        if (!ObjectUtils.isEmpty(birthdate))
//            filter.setBirthdate(birthdate);
//        if (!ObjectUtils.isEmpty(minScholarship))
//            filter.setMinScholarship(minScholarship);
//        if (!ObjectUtils.isEmpty(maxScholarship))
//            filter.setMaxScholarship(maxScholarship);
//        if (!ObjectUtils.isEmpty(country))
//            filter.setCountry(country);
//        if (!ObjectUtils.isEmpty(city))
//            filter.setCity(city);
//        if (!ObjectUtils.isEmpty(addressLine))
            filter.setAddressLine(teacherName);
        return filter;
    }
}
